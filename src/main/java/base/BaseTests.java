package base;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByTagName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTests {

	private WebDriver driver;
	
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", "resorces/chromedriver");
		driver = new ChromeDriver();
		driver.get("http://the-internet.herokuapp.com/");
		driver.findElement(By.linkText("Shifting Content")).click();

		driver.findElement(By.linkText("Example 1: Menu Element")).click();
		System.out.println("There are " + driver.findElements(By.tagName("li")).size()+ " Menu Items");
		
	}
	
	public static void main(String args[]) {
		BaseTests test = new BaseTests();
		test.setUp();
	}
}
